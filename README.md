# Interwiki Formatting Dokuwiki Plugin

[![MIT License](https://svgshare.com/i/TRb.svg)](https://opensource.org/licenses/MIT)
[![DokuWiki Plugin](https://svgshare.com/i/TSa.svg)](https://www.dokuwiki.org/dokuwiki)
[![Plugin Home](https://svgshare.com/i/TRw.svg)](https://www.dokuwiki.org/plugin:interwikiformatting)
[![Gitlab Repo](https://svgshare.com/i/TRR.svg)](https://gitlab.com/JayJeckel/interwikiformatting)
[![Gitlab Issues](https://svgshare.com/i/TSw.svg)](https://gitlab.com/JayJeckel/interwikiformatting/issues)
[![Gitlab Download](https://svgshare.com/i/TT5.svg)](https://gitlab.com/JayJeckel/interwikiformatting/-/archive/master/interwikiformatting-master.zip)

Interwiki links are a powerful and time saving tool in the belt of any Dokuwiki editor, especially on wikis that provide various custom shortcuts to often used sites. However, they do have one drawback, namely that you either have to accept the displaying of the URL part in whatever form is required for the related site or you have to constantly add titles that more often than not only differ from the URL part by their formatting.

This plugin attempts to solve that issue by automating the formatting of interwiki links that don't have a title manually defined. For sites like Tv Tropes where page ids are composed of multiple words run together without spaces the plugin will reinsert them and on sites like Wikipedia where page ids are made of words separated by underscores or other delimiters the plugin will replace them with spaces. The displayed title will then have the capitalization of words standardized into common title case where all words except articles, conjunctions, prepositions, and uppercase acronyms start with a capital letter.

This automation of formatting and the configurability provided by the plugin's settings should simplify the use of interwiki links, standardize their displayed appearance, and save time for any editor using them.

## Installation

Search and install the plugin using the [Extension Manager](https://www.dokuwiki.org/plugin:extension) or install directly using the latest [download url](https://gitlab.com/JayJeckel/interwikiformatting/-/archive/master/interwikiformatting-master.zip), otherwise refer to [Plugins](https://www.dokuwiki.org/plugins) on how to install plugins manually.

## Usage

This plugin requires no special syntax, simply use interwiki links, *[[shortcut>Some_Page#OptionalAnchor]]*, as normal and the automatic formatting will be applied unless the interwiki link already has a title defined, *[[shortcut>Some_Page#OptionalAnchor|No Formatting Applied Here Because I Already Have A Title]]*.

## Configuration Settings

The Interwiki Formatting Plugin has eight settings that can be set in the Dokuwiki Configuration Manager:

  - **Ignored Shortcut List** (*ignored_shortcuts*) - A space separated list of interwiki shortcuts that should not receive any automatic formatting. The default list includes the *user* and *skype* shortcuts.
  - ** Strip Query Toggle** (*strip_query*) - Whether or not the query part of the interwiki link should be stripped from the displayed title. This setting is enabled by default.
  - **Strippable Extension List** (*strippable_extensions*) - A space separated list of extensions that should be removed from the end of interwiki link titles. The default list includes *asp*, *htm*, *html*, and *php*.
  - **Word Separator List** (*word_separators*) - A space separated list of characters that should be replaced by a space when the title is formatted. The default list includes *_*, *:*, *.*, and *-*.
  - **Anchor Replacement Text** (*anchor_replacement*) - The text that the anchor symbol (#) will be replaced by unless it is the first or last character of the string, in which case it will be remove entirely. The default value is a dash with a space on both sides (* - *).
  - **Slash Replacement Text** (*slash_replacement*) - The text that the common path separator symbol (/) will be replaced by unless it is the first or last character of the string, in which case it will be remove entirely. The default value is a dash with a space on both sides (* - *).
  - **Capitalize Words Toggle** (*capitalize_words*) - Whether or not all words in the display interwiki link title should be capitalized. This setting is enabled by default.
  - **Capitalize Exceptions List** (*capitalize_exceptions*) - A space separated list of words that should not be capitalized. This setting is ignored if capitalizing of words is disabled. The words in this list will still be capitalized if they are the first or last word of the title, the first word after the anchor or slash replacement text, or the last word before the anchor of slash replacement text. The default word list includes *the*, *of*, *a*, *an*, *and*, *but*, *or*, *for*, *nor*, *are*, *in*, and *at*.

## Security

There are currently no know vulnerabilities related to normal use of the mod. The mod takes advantage of the built-in methods for creating interwiki links and simple provides them with a generated title in lieu of a user-defined one. This should be no more or less secure than any normal interwiki link. No extra measures have been implemented to secure or validate the values of plugin's configuration settings, but these should only be available to the wiki's admin and those user's who have trusted with access to the configuration manager, so they are no more or less secure than any other setting.
