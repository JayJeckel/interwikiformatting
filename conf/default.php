<?php
/*
 * Interwiki Formatting Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

$conf['ignored_shortcuts'] = 'user skype';
$conf['strip_query'] = 1;
$conf['strippable_extensions'] = 'asp htm html php';
$conf['word_separators'] = '_ : . -';
$conf['anchor_replacement'] = ' - ';
$conf['slash_replacement'] = ' - ';
$conf['capitalize_words'] = 1;
$conf['capitalize_exceptions'] = 'the of a an and but or for nor are in at';

?>