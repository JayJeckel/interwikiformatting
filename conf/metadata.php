<?php
/*
 * Interwiki Formatting Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

$meta['ignored_shortcuts'] = array('string');
$meta['strip_query'] = array('onoff');
$meta['strippable_extensions'] = array('string');
$meta['word_separators'] = array('string');
$meta['anchor_replacement'] = array('string');
$meta['slash_replacement'] = array('string');
$meta['capitalize_words'] = array('onoff');
$meta['capitalize_exceptions'] = array('string');

?>