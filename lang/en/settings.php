<?php
/*
 * Interwiki Formatting Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

$lang['ignored_shortcuts'] = "Space separated list of interwiki shortcuts that should not be automatically formatted. Default: 'user'";
$lang['strip_query'] = "Should the query part of the interwiki url be stripped? Default: Yes";
$lang['strippable_extensions'] = "Space separated list of extensions that should be stripped from the formatted title. Default: 'asp htm html php'";
$lang['word_separators'] = "Space separated list of characters that should be replaced with a space. Default: '_ : . -'";
$lang['anchor_replacement'] = "What text should replace the standard url anchor symbol (#) in the display title? Default: ' - '";
$lang['slash_replacement'] = "What text should replace the standard path separator symbol (/) in the display title? Default: ' - '";
$lang['capitalize_words'] = "Should the first letter of each word be capitalized? Default: Yes";
$lang['capitalize_exceptions'] = "Space separated list of words that will not be capitalized if the above option is checked. Default: 'the of a an and but or for nor are in'";

?>